# Quiz
#dart #flutter

Hi, guys! This app is a quiz. It offers to take a short test from different it-categories. The application has a connection to an [API server](https://quizapi.io/) that generates questions.

# View app 

<h3>1. Category page</h4>
<h4>Here you can choose the category and level of difficulty for the test.</h4>
<p align="center"><img src="./screens/1.png" width="40%"></p>

<h3>2. Question page</h4>
<h4>Screen with questions.</h4>
<p align="center"><img src="./screens/2.png" width="40%"></p>

<h3>3. Score page</h4>
<h4>Result.</h4>
<p align="center"><img src="./screens/3.png" width="40%"></p>

## Flutter packages

- [json_serializable (6.3.1)](https://pub.dev/packages/json_serializable)
- [bloc (8.1.0)](https://pub.dev/packages/bloc)
- [flutter_bloc (8.1.0)](https://pub.dev/packages/flutter_bloc)

## Unit test

This application has tests aimed at checking the connection with the API service. (test folder)

