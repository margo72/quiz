import "package:json_annotation/json_annotation.dart";

part 'question.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Question {
  final int id;
  final String question;
  final String? description;
  final Answers answers;
  @JsonKey(fromJson: stringToBool)
  final bool multipleCorrectAnswers;
  final CorrectAnswers correctAnswers;
  final String? correctAnswer;
  final String? explanation;
  final List<Tag> tags;
  final String? category;
  final String difficulty;

  Question(
    this.id,
    this.question,
    this.answers,
    this.correctAnswer,
    this.explanation,
    this.tags,
    this.category,
    this.difficulty,
    this.multipleCorrectAnswers,
    this.description,
    this.correctAnswers,
  );

  factory Question.fromJson(Map<String, dynamic> json) =>
      _$QuestionFromJson(json);

  Map<String, dynamic> toJson() => _$QuestionToJson(this);
}

@JsonSerializable()
class Answers {
  @JsonKey(name: 'answer_a')
  final String? a;
  @JsonKey(name: 'answer_b')
  final String? b;
  @JsonKey(name: 'answer_c')
  final String? c;
  @JsonKey(name: 'answer_d')
  final String? d;
  @JsonKey(name: 'answer_e')
  final String? e;
  @JsonKey(name: 'answer_f')
  final String? f;

  Answers(
    this.a,
    this.b,
    this.c,
    this.d,
    this.e,
    this.f,
  );

  factory Answers.fromJson(Map<String, dynamic> json) =>
      _$AnswersFromJson(json);

  Map<String, dynamic> toJson() => _$AnswersToJson(this);
}

@JsonSerializable()
class CorrectAnswers {
  @JsonKey(name: 'answer_a_correct', fromJson: stringToBool)
  final bool a;
  @JsonKey(name: 'answer_b_correct', fromJson: stringToBool)
  final bool b;
  @JsonKey(name: 'answer_c_correct', fromJson: stringToBool)
  final bool c;
  @JsonKey(name: 'answer_d_correct', fromJson: stringToBool)
  final bool d;
  @JsonKey(name: 'answer_e_correct', fromJson: stringToBool)
  final bool e;
  @JsonKey(name: 'answer_f_correct', fromJson: stringToBool)
  final bool f;

  CorrectAnswers(
    this.a,
    this.b,
    this.c,
    this.d,
    this.e,
    this.f,
  );

  factory CorrectAnswers.fromJson(Map<String, dynamic> json) =>
      _$CorrectAnswersFromJson(json);

  Map<String, dynamic> toJson() => _$CorrectAnswersToJson(this);
}

@JsonSerializable()
class Tag {
  final String name;

  Tag(this.name);

  factory Tag.fromJson(Map<String, dynamic> json) => _$TagFromJson(json);

  Map<String, dynamic> toJson() => _$TagToJson(this);
}

bool stringToBool(String? value) => value == true.toString();
