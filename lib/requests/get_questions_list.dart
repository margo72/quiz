import 'dart:convert';
import 'package:quiz/question.dart';
import 'package:http/http.dart' as http;

Future<List<Question>> getQuestionsList(
    String category, String difficulty) async {
  String apiKey = 'j24WhINsXuMG7PszLmbkLHqRiXRoFnjRZrHxkwDa';
  try {
    final url = Uri.parse(
        'https://quizapi.io/api/v1/questions?apiKey=$apiKey&category=$category&difficulty=$difficulty&limit=10');
    var request = http.MultipartRequest("GET", url);
    final response = await http.Response.fromStream(await request.send());
    Iterable l = json.decode(response.body);
    List<Question> questions =
        List<Question>.from(l.map((model) => Question.fromJson(model)));

    return questions;
  } catch (error) {
    return [];
  }
}
