import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quiz/modules/question/bloc/question_bloc.dart';
import 'package:quiz/modules/question/bloc/question_event.dart';
import 'package:quiz/modules/question/bloc/question_state.dart';
import 'package:quiz/modules/widgets/show_answer.dart';
import 'package:quiz/modules/widgets/main_button.dart';
import 'package:quiz/modules/widgets/title_text.dart';
import 'package:quiz/question.dart';

class QuestionPage extends StatelessWidget {
  const QuestionPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context)!.settings.arguments as Map;
    final String category = arguments['category'];
    final String difficulty = arguments['difficulty'];
    return BlocProvider(
      create: (BuildContext context) {
        return QuestionBloc();
      },
      child: QuestionPageView(
        category: category,
        difficulty: difficulty,
      ),
    );
  }
}

class QuestionPageView extends StatefulWidget {
  final String category;
  final String difficulty;

  const QuestionPageView({
    Key? key,
    required this.category,
    required this.difficulty,
  }) : super(key: key);

  @override
  State<QuestionPageView> createState() => _QuestionPageViewState();
}

class _QuestionPageViewState extends State<QuestionPageView> {
  @override
  void initState() {
    context.read<QuestionBloc>().add(LoadingQuestionEvent(
          category: widget.category,
          difficulty: widget.difficulty,
        ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Questions'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: BlocConsumer<QuestionBloc, QuestionState>(
          builder: (context, state) {
            if (state is LoadingQuestionState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is EmptyQuestionState) {
              return const EmptyQuestion();
            }

            if (state is ErrorLoadingQuestionState) {
              return Center(
                child: Container(
                  color: Colors.red,
                  child: Text(
                    state.errorText,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              );
            }
            if (state is AnswersQuestionState) {
              return const ShowQuestions();
            }
            return Container();
          },
          buildWhen: (previous, state) {
            return true;
          },
          listener: (context, state) {
            if (state is AnswersQuestionState && state.errorText != null) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.errorText!),
                ),
              );
            }
          },
          listenWhen: (context, current) {
            return current is AnswersQuestionState;
          },
        ),
      ),
    );
  }
}

class EmptyQuestion extends StatelessWidget {
  const EmptyQuestion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Вопросы не найдены'),
    );
  }
}

class ShowQuestions extends StatelessWidget {
  const ShowQuestions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuestionBloc, QuestionState>(
      builder: (context, state) => state is AnswersQuestionState &&
              state.questions != null
          ? Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        ShowStarsForQuestions(
                          answers: state.answers,
                          questionsIndex: state.index,
                        ),
                        ShowQuestion(
                          question: state.questions![state.index],
                          userCorrectAnswersMap: state.userCorrectAnswersMap,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: state.answers[9] != null
                        ? MainButton(
                            "Finish the test",
                            onPressed: () {
                              Navigator.pushNamed(context, '/score',
                                  arguments: state.answers
                                      .where((element) => element == true)
                                      .toList()
                                      .length);
                            },
                          )
                        : const SizedBox(),
                  ),
                ],
              ),
            )
          : Container(),
    );
  }
}

class ShowStarsForQuestions extends StatelessWidget {
  final List<bool?> answers;
  final int questionsIndex;

  const ShowStarsForQuestions({
    Key? key,
    required this.answers,
    required this.questionsIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List<Widget>.generate(
          answers.length,
          (index) => Icon(
            Icons.star,
            color: answers[index] == null
                ? index == questionsIndex
                    ? Colors.amber
                    : Colors.grey
                : answers[index] == true
                    ? Colors.blue
                    : Colors.red,
          ),
        ),
      ),
    );
  }
}

class ShowQuestion extends StatefulWidget {
  final Question question;
  final Map<String, bool?> userCorrectAnswersMap;

  const ShowQuestion({
    Key? key,
    required this.question,
    required this.userCorrectAnswersMap,
  }) : super(key: key);

  @override
  State<ShowQuestion> createState() => _ShowQuestionState();
}

class _ShowQuestionState extends State<ShowQuestion> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      key: ValueKey(widget.question.id),
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        TitleText(widget.question.question),
        const SizedBox(height: 10.0),
        ShowAnswer(
          answer: widget.question.answers.a,
          color: widget.question.correctAnswers.a == true
              ? Colors.blue
              : Colors.red,
          onTap: () {
            Future.delayed(const Duration(milliseconds: 500), () {
              context.read<QuestionBloc>().add(
                    AnswerQuestionEvent2(widget.question.correctAnswers.a),
                  );
            });
          },
        ),
        ShowAnswer(
          answer: widget.question.answers.b,
          color: widget.question.correctAnswers.b == true
              ? Colors.blue
              : Colors.red,
          onTap: () {
            Future.delayed(const Duration(milliseconds: 500), () {
              context.read<QuestionBloc>().add(
                    AnswerQuestionEvent2(widget.question.correctAnswers.b),
                  );
            });
          },
        ),
        ShowAnswer(
          answer: widget.question.answers.c,
          color: widget.question.correctAnswers.c == true
              ? Colors.blue
              : Colors.red,
          onTap: () {
            Future.delayed(const Duration(milliseconds: 500), () {
              context.read<QuestionBloc>().add(
                    AnswerQuestionEvent2(widget.question.correctAnswers.c),
                  );
            });
          },
        ),
        ShowAnswer(
          answer: widget.question.answers.d,
          color: widget.question.correctAnswers.d == true
              ? Colors.blue
              : Colors.red,
          onTap: () {
            Future.delayed(const Duration(milliseconds: 500), () {
              context.read<QuestionBloc>().add(
                    AnswerQuestionEvent2(widget.question.correctAnswers.d),
                  );
            });
          },
        ),
        ShowAnswer(
          answer: widget.question.answers.e,
          color: widget.question.correctAnswers.e == true
              ? Colors.blue
              : Colors.red,
          onTap: () {
            Future.delayed(const Duration(milliseconds: 500), () {
              context.read<QuestionBloc>().add(
                    AnswerQuestionEvent2(widget.question.correctAnswers.e),
                  );
            });
          },
        ),
        ShowAnswer(
          answer: widget.question.answers.f,
          color: widget.question.correctAnswers.f == true
              ? Colors.blue
              : Colors.red,
          onTap: () {
            Future.delayed(const Duration(milliseconds: 500), () {
              context.read<QuestionBloc>().add(
                    AnswerQuestionEvent2(widget.question.correctAnswers.f),
                  );
            });
          },
        ),
        Container(
          child: widget.question.multipleCorrectAnswers
              ? const Text('*The question has several possible answers')
              : null,
        ),
      ],
    );
  }
}
