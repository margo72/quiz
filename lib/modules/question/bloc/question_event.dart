//события
abstract class QuestionEvent {}

class LoadingQuestionEvent extends QuestionEvent {
  final String category;
  final String difficulty;

  LoadingQuestionEvent({
    required this.category,
    required this.difficulty,
  });
}

class AnswerQuestionEvent extends QuestionEvent {
  final Map<String, bool?> userAnswersMap;
  Map<String, bool> correctAnswersMap;

  AnswerQuestionEvent(
      {required this.userAnswersMap, required this.correctAnswersMap});
}

class AnswerQuestionEvent2 extends QuestionEvent {
  final bool answer;

  AnswerQuestionEvent2(this.answer);
}
