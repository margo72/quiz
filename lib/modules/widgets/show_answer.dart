import 'package:flutter/material.dart';

class ShowAnswer extends StatefulWidget {
  final String? answer;
  final VoidCallback? onTap;
  final Color color;
  final bool showColor;

  const ShowAnswer({
    Key? key,
    required this.answer,
    this.onTap,
    required this.color,
    bool? showColor,
  })  : showColor = showColor ?? false,
        super(key: key);

  @override
  State<ShowAnswer> createState() => _ShowAnswerState();
}

class _ShowAnswerState extends State<ShowAnswer> {
  late bool clicking;

  @override
  void initState() {
    setState(() {
      clicking = widget.showColor;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.answer != null) {
      return GestureDetector(
        onTap: () {
          setState(() => clicking = true);
          widget.onTap?.call();
        },
        child: ConstrainedBox(
          constraints: const BoxConstraints.tightFor(width: double.infinity),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5.0),
            margin: const EdgeInsets.symmetric(vertical: 2),
            color: clicking == true ? widget.color : Colors.black12,
            child: Text(
              widget.answer.toString(),
              style: const TextStyle(fontSize: 16.0),
            ),
          ),
        ),
      );
    } else {
      return const SizedBox();
    }
  }
}
