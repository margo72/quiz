import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;

  const MainButton(
    this.text, {
    this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        primary: Colors.white,
        backgroundColor: Colors.blue,
        elevation: 2,
      ),
      onPressed: onPressed,
      child: Container(
        padding: const EdgeInsets.all(2.0),
        width: MediaQuery.of(context).size.width,
        child: Center(
            child: Text(
          text,
          style: const TextStyle(fontSize: 18),
        )),
      ),
    );
  }
}
