import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quiz/modules/category/bloc/category_bloc.dart';
import 'package:quiz/modules/category/bloc/category_state.dart';
import 'package:quiz/modules/category/widgets/choice_category.dart';
import 'package:quiz/modules/widgets/main_button.dart';
import 'package:quiz/modules/widgets/title_text.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return CategoryBloc();
      },
      child: const CategoryPageView(),
    );
  }
}

class CategoryPageView extends StatefulWidget {
  const CategoryPageView({Key? key}) : super(key: key);

  @override
  State<CategoryPageView> createState() => _CategoryPageViewState();
}

class _CategoryPageViewState extends State<CategoryPageView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Select a category'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: BlocConsumer<CategoryBloc, CategoryState>(
          builder: (context, state) {
            return Center(
              child: Column(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        const SizedBox(height: 4.0),
                        const TitleText("select a category"),
                        const SizedBox(height: 4.0),
                        ChoiceCategory(
                          list: Parameters.categoryList,
                          index: state.indexCategory,
                        ),
                        const SizedBox(height: 4.0),
                        const TitleText("select difficulty"),
                        const SizedBox(height: 4.0),
                        ChoiceCategory(
                          list: Parameters.difficultyList,
                          index: state.indexDifficulty,
                        ),
                      ],
                    ),
                  ),
                  MainButton(
                    "Begin",
                    onPressed: () {
                      Navigator.pushNamed(context, '/question', arguments: {
                        'category':
                            Parameters.categoryList[state.indexCategory],
                        'difficulty':
                            Parameters.difficultyList[state.indexDifficulty],
                      });
                    },
                  ),
                ],
              ),
            );
          },
          buildWhen: (previous, state) {
            return true;
          },
          listener: (context, state) {
            if (state is ErrorCategoryState) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.errorText),
                ),
              );
            }
          },
          listenWhen: (context, current) {
            return current is ErrorCategoryState;
          },
        ),
      ),
    );
  }
}
