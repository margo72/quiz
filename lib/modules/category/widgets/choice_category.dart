import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quiz/modules/category/bloc/category_bloc.dart';
import 'package:quiz/modules/category/bloc/category_event.dart';
import 'package:quiz/modules/widgets/show_answer.dart';

class ChoiceCategory extends StatelessWidget {
  final List list;
  final int index;

  const ChoiceCategory({
    super.key,
    required this.list,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List<Widget>.generate(
        list.length,
        (indexList) => ShowAnswer(
          answer: list[indexList],
          color: indexList == index ? Colors.blue : Colors.black12,
          showColor: true,
          onTap: () {
            context.read<CategoryBloc>().add(
                  ChoiceCategoryEvent(
                    value: list[indexList],
                  ),
                );
          },
        ),
      ),
    );
  }
}
