abstract class Parameters {
  static const List categoryList = [
    'Linux',
    'DevOps',
    'Networking',
    'Programming (PHP, JS, Python and etc.)',
    'Cloud',
    'Docker',
    'Kubernetes',
    'And lots more',
  ];
  static const List difficultyList = [
    'Easy',
    'Medium',
    'Hard',
  ];
}

abstract class CategoryState with Parameters {
  final int indexCategory;
  final int indexDifficulty;

  CategoryState({
    required this.indexCategory,
    required this.indexDifficulty,
  });
}

class LoadingCategoryState extends CategoryState {
  LoadingCategoryState({
    required super.indexCategory,
    required super.indexDifficulty,
  });
}

class ChoiceCategoryState extends CategoryState {
  final int indexcategory;
  final int indexdifficulty;

  ChoiceCategoryState({
    int? indexCategory,
    int? indexDifficulty,
  })  : indexcategory = indexCategory ?? 0,
        indexdifficulty = indexDifficulty ?? 0,
        super(
          indexCategory: indexCategory ?? 0,
          indexDifficulty: indexDifficulty ?? 0,
        );
}

class ErrorCategoryState extends CategoryState {
  final String errorText;

  ErrorCategoryState(
    this.errorText, {
    required super.indexCategory,
    required super.indexDifficulty,
  });
}
