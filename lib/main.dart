import 'package:flutter/material.dart';
import 'package:quiz/modules/category/category_page.dart';
import 'package:quiz/modules/question/question_page.dart';
import 'package:quiz/modules/score/score_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (context) => const CategoryPage(),
        '/question': (context) => const QuestionPage(),
        '/score': (context) => const ScorePage(),
      },
      //home: const CategoryPage(),
    );
  }
}
